<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>Магазин</title>
</head>
<body>
    <!-- <div class="header">
        <ul class="nav">
            <li class="home"><a href="index.php">Home</a></li>
            <li><a href="register.php">Sign Up</a></li>
            <li><a href="signin.php">Sign In</a></li>
        </ul>
    </div> -->

    <nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
        <a class="navbar-brand" href="index.php">Магазин</a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="basket.php">Корзина</a></li>
            <li><a href="register.php">Регистрация</a></li>
            <li><a href="signin.php">Вход</a></li>
        </ul>
    </div>
        </nav>        
  
    <!-- <form class="form" action="reg.php" method="POST">
        
        <input type="text" name="username" placeholder="username" required>
        <input type="text" name="password" placeholder="password" required>
        <input type="email" name="email" placeholder="email" required>
        <input type="submit" name="submit" value="Register">
    </form> -->

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <form action="reg.php" method="POST" class="form-group">
                    <div class="form-group">
                            <input type="text" class="form-control" name="username" placeholder="username" required>
                    </div>
                    <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="password" required>
                    </div>
                    <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="email" required>
                    </div>
                    <input class="btn btn-success " type="submit" name="submit" value="Register">
                    <!-- <button class="btn btn-success ">SIGN UP</button> -->
            </form>
        </div>
        <div class="col-md-2"></div>
    </div>

</body>
</html>