<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="style.css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<title>Магазин</title>
</head>
<body>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
            <a class="navbar-brand" href="index.php">Магазин</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="basket.php">Корзина</a></li>
                <li><a href="register.php">Регистрация</a></li>
                <li><a href="signin.php">Вход</a></li>
            </ul>
        </div>
    </nav>
  
    <ul class="list">
    <?php
        require('connect.php');
        $username = $_SESSION['login'];
        $query = "select * from products1 where id in 
                        (
                        SELECT product_id 
                        FROM basket 
                        where user_name = '".$username."'
                        );
                        ";

        $result = mysqli_query($connection,$query);

        while($row = mysqli_fetch_assoc($result)){?>
            <li class="list-item">
                <div class="jumbotron">
                    <h3 class="name"><?php echo $row['name'];?></h3>
                    <div class="image"><img src="<?php echo $row['image'];?>" width="180px"  alt="cook"></div>
                    <h4 class="price" ><?php echo $row['price'];?></h4>
                        <form method = "POST" action="delete_from_basket.php">
                            <input type="hidden" name="product_id" value="<?php echo $row['id'];?>">
                            <p><input class="btn btn-success btn-block"  type="submit"  value="Удалить из корзины"></p>
                        </form>
                </div>
            </li>
    
    <?php } ?>
        </ul>
</body>
</html>


