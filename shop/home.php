<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <title>Home</title>
</head>
<body>
 <div class="header">
  <ul class="nav">
   <li><a href="index.php">Home</a></li>
   <li><a href="basket.php">Your Basket</a></li>
   <li><a href="register.php">Sign Up</a></li>
   <li><a href="signin.php">Sign In</a></li>
  </ul>
 </div>

    <h1>Welcome home!</h1>
    <a href="addproduct.php">Add product</a>
    <a href="logout.php">Logout</a>
</body>
</html>