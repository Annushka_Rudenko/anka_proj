<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="style.css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<title>Магазин</title>
</head>
<body>
<!-- <div class="header">
    <ul class="nav">
        <li class="home"><a href="index.php">Домашняя страница</a></li>
        <li><a href="basket.php">Your Basket</a></li>
        <li><a href="register.php">Sign Up</a></li>
        <li><a href="signin.php">Sign In</a></li>
    </ul>
</div> -->

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
        <a class="navbar-brand" href="index.php">Магазин</a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="basket.php">Корзина</a></li>
            <li><a href="register.php">Регистрация</a></li>
            <li><a href="signin.php">Вход</a></li>
        </ul>
    </div>
</nav> 
<h1>Товар был ранее добавлен в корзину</h1>
<button type="button" class="btn btn-default"><a href='index.php'>Перейти на главную страницу!</a></button>

</body>
</html>