-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: shop1
-- ------------------------------------------------------
-- Server version	5.5.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `products1`
--

DROP TABLE IF EXISTS `products1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `description` varchar(155) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products1`
--

LOCK TABLES `products1` WRITE;
/*!40000 ALTER TABLE `products1` DISABLE KEYS */;
INSERT INTO `products1` VALUES (1,'Samsung Galaxy J5',7999.00,'Экран (5.2\", Super AMOLED, 1280x720)/ Exynos 7870 (1.6 ГГц)/ основная камера: 13 Мп, фронтальная камера: 13 Мп/ RAM 2 ГБ/ 16 ГБ встроенной памяти + microSD','img/1.jpg'),(2,'Huawei Mate 10 Lite ',9999.00,'dver platich','img/2.jpg'),(3,'LG G6 Black',19999.00,'dver paperov','https://i2.rozetka.ua/goods/1892329/copy_lg_lgh845_acistn_58d8fc4a87d51_images_1892329883.jpg'),(4,'Huawei P8 Lite 2017',5555.00,'dver glassnya','https://i1.rozetka.ua/goods/1838558/huawei_p8_lite_2017_white_images_1838558073.jpg'),(5,'Apple iPhone 8 Plus',33999.00,'','https://i1.rozetka.ua/goods/2356300/apple_iphone_8_plus_256gb_space_gray_images_2356300961.jpg'),(6,'Asus Zenfone 3',7499.00,'','https://i2.rozetka.ua/goods/1792342/asus_zenfone_3_ze520kl_black_images_1792342449.jpg'),(7,'Apple iPhone 7 ',17999.00,'','https://i2.rozetka.ua/goods/1748261/apple_iphone_7_32gb_gold_images_1748261327.jpg'),(8,'Huawei Nova ',7999.00,'','https://i1.rozetka.ua/goods/1780842/huawei_nova_gold_images_1780842114.jpg'),(9,'Samsung Galaxy A3',9499.00,'','https://i2.rozetka.ua/goods/1826767/copy_samsung_galaxy_a3_2017_duos_sm_a320_16gb_blue_586cf8760777a_images_1826767131.jpg'),(10,'Nokia 8 Dual',18999.00,'','https://i1.rozetka.ua/goods/2152135/nokia_8_ds_blue_images_2152135231.jpg');
/*!40000 ALTER TABLE `products1` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-21  0:13:45
